import { faGithub, faQq, faTwitter, faWeibo, faWeixin } from '@fortawesome/free-brands-svg-icons'
import { faCircle, faDotCircle } from '@fortawesome/free-regular-svg-icons'
import {
  faBookOpen,
  faCircleNotch,
  faComment,
  faComments,
  faFeatherAlt,
  faGlasses,
  faHistory,
  faMusic,
  faSubway,
  faUserFriends,
  faVideo,
} from '@fortawesome/free-solid-svg-icons'
import { MenuModel, SocialLinkModel } from 'common/store/types'
const menu: MenuModel[] = [
  {
    title: '主页',
    path: '/',
    type: 'Home',
    icon: faDotCircle,
    subMenu: [],
  },
  {
    title: '文章',
    path: '/posts',
    type: 'Post',
    subMenu: [],
    icon: faGlasses,
  },
  {
    title: '随笔',
    type: 'Note',
    path: '/notes',
    icon: faFeatherAlt,
  },
  {
    title: '闲言',
    path: '/says',
    icon: faComments,
  },
  {
    title: '历史',
    icon: faHistory,
    path: '/timeline',
    subMenu: [
      {
        title: '博文',
        icon: faBookOpen,
        path: '/timeline?type=post',
      },
      {
        title: '随笔',
        icon: faFeatherAlt,
        path: '/timeline?type=note',
      },
      {
        title: '时间线',
        icon: faCircle,
        path: '/timeline?memory=1',
      },
    ],
  },
  {
    title: '友链',
    icon: faUserFriends,
    path: '/friends',
  },
  {
    title: '动态',
    icon: faComment,
    path: '/recently',
  },
  {
    title: '其他',
    icon: faCircleNotch,
    path: '/favorite/music',
    subMenu: [
      {
        title: '听歌',
        icon: faMusic,
        type: 'Music',
        path: '/favorite/music',
      },
      // {
      //   title: '看番',
      //   icon: faTv,
      //   type: 'Bangumi',
      //   path: '/favorite/bangumi',
      // },
      // {
      //   title: '项目',
      //   icon: faFlask,
      //   type: 'Project',
      //   path: '/projects',
      // },
    ],
  },
  {
    title: '',
    icon: faSubway,
    path: 'https://travellings.link',
  },
]
const social: SocialLinkModel[] = [
  {
    url: 'https://github.com/libin47',
    title: 'GitHub',
    icon: faGithub,
    color: 'var(--black)',
  },
  {
    url: 'https://qm.qq.com/cgi-bin/qm/qr?k=cietrvj0rtlqlpJ5d997T-VnN_WPJvwg&jump_from=webapi',
    title: 'QQ',
    icon: faQq,
    color: '#12b7f5',
  },
  {
    url: 'https://weibo.com/u/2415620797',
    title: 'WeiBo',
    icon: faWeibo,
    color: 'red',
  },
  {
    url: 'https://live.bilibili.com/8459759',
    title: 'BiliBili',
    icon: faVideo,
    color: 'yellow',
  },
  // {
  //   url: 'https://twitter.com/__oQuery',
  //   title: 'twitter',
  //   icon: faTwitter,
  //   color: '#02A4ED',
  // },
]
export default {
  url: 'https://wind-watcher.cn',
  alwaysHTTPS:
    process.env.NODE_ENV === 'development'
      ? false
      : process.env.NEXT_PUBLIC_ALWAYS_HTTPS &&
        parseInt(process.env.NEXT_PUBLIC_ALWAYS_HTTPS) === 1,
  social,
  biliId: 30001887,
  homePage: 'https://wind-watcher.cn', // footer link
  menu,
  icp: {
    name: '冀ICP备2021026266号-1',
    url: 'https://beian.miit.gov.cn/',
  },
  travellings: true, // 开往
  donate: 'https://afdian.net/@Innei',
}
