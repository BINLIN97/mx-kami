# Kami

**NOTE: 此版本专为 [v3.0 版的 Server](https://github.com/mx-space/server-next) 打造, [旧版 Server](https://github.com/mx-space/server) 请使用 kami [v1.18](https://github.com/mx-space/kami/tree/v1.18.0)**

[![DeepScan grade](https://deepscan.io/api/teams/7938/projects/10822/branches/154495/badge/grade.svg)](https://deepscan.io/dashboard#view=project&tid=7938&pid=10822&bid=154495)
[![wakatime](https://wakatime.com/badge/github/mx-space/kami.svg)](https://wakatime.com/badge/github/mx-space/kami)

**你的下一个博客何必是博客**

Kami 是为 Mix Space Server 打造的前端. 使用 NextJS 开发.

Live Demo:

- <https://innei.ren>

![Xnip2021-09-21_18-29-08](https://raw.githubusercontent.com/mx-space/docs-images/master/images/Xnip2021-09-21_18-29-08.png)

[使用文档](https://mx-docs.shizuri.net)

## 忠告

可以在此基础上保留署名进行二次创作，但是禁止用于以盈利为目的一切活动。

## 许可

此项目 GPLv3 授权开源，使用此项目进行的二次创作或者衍生项目也必须开源。
